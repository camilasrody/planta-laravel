<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>


## Planta-Laravel
### pt-br

Este é um projeto de software destino acadêmico relacionado ao trabalho de conclusão de curso (**TCC**) de Sistemas de informação.
O tema do projeto aborda sustentabilidade, automação e aplicação web/app.
Automação de rega para manter a saúde de temperos e hortaliças.

Nele contém lógicas elaboradas para enviar comandos e interagir com a automação.
Contém:
- Criação de Web server para receber as informações.
- lógica para regar duas vezes ao dia.
- lógica para agendar horário de irrigação.
- lógica para criar histórico relacionado ao usuário.
- lógica para controle de dados utilizando banco de dados MariaDB.
- lógica para orquestrar a umidade e tomar notificar o usuário.

### eng.
This is an academic destination software project related to the conclusion work of Information Systems.
The project's theme addresses sustainability, automation and web / app application.
Watering automation to maintain the health of spices and vegetables.  
It contains logic designed to interact, send commands and interact with automation.
Contains:
- Web server creation to receive information.
- logic for watering twice a day.
- logic to schedule irrigation schedule.
- logic to create user-related history.
- data control logic using MariaDB database.
- logic to orchestrate humidity and take to notify the user.

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 1500 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.


## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
